# Superset Helm Chart

## preparing secrets 

 
        ``` script
        kubectl create secret generic redisdb  --from-literal redis-password=xxxxxxxx
          kubectl create secret generic db-postgresql  --from-literal postgres-password=xxxxxxxx
        
        ```


## running the chart 

in order to install this chart with it's default database run the following commande :  
    
        ``` script
        helm upgrade --install  superset ./superset
        
        ```
   
in order to install this chart with your own postgres db run the following commande : 

``` script
       helm upgrade --install --values my-values.yaml  superset  superset

```

this commande  will override the default posgtres db and redis parameters with the ones specified in my-values.yaml file 


  

