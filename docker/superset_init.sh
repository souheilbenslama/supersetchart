#!/bin/sh
set -eu
echo "Upgrading DB schema..."
superset db upgrade ;
echo "Initializing roles..."
superset init ;
if [ "$INIT_CREATE_ADMIN" = true ];then
  echo "Creating admin user..."
  superset fab create-admin \
                --username $INIT_ADMIN_USERNAME \
                --firstname $INIT_ADMIN_FIRSTNAME \
                --lastname $INIT_ADMIN_LASTNAME \
                --email $INIT_ADMIN_EMAIL \
                --password $INIT_ADMIN_PASSWORD \
                || true
fi



if [   "$INIT_LOAD_EXAMPLES" = "true" ];then
  echo "Loading examples..."
  superset load_examples ;
fi
if [ -f "/app/pythonpath/import_datasources.yaml" ]; then
  echo "Importing database connections.... "
  superset import_datasources -p /app/pythonpath/import_datasources.yaml
fi